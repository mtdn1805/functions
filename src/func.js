const getSum = (str1, str2) => {
  if (typeof(str1) !== 'string' || typeof(str2) !== 'string') {
    return false
  } else {
    return (isNaN(+str1) || isNaN(+str2)) ? false : +str1 + +str2 + '';
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let comms = 0;
  let posts = 0;
  for (let obj of listOfPosts) {
    if (obj.author === authorName) {
      posts++;
    }
    if (obj.comments) {
      for (let item of obj.comments) {
        if (item.author === authorName) {
         comms++;
        }
      }
    }
  }
  return `Post:${posts},comments:${comms}`;
};

const tickets=(people)=> {
  let status = false;
  let clercMoney = 0;
  let ticketCost = 25;
  if (people[0] != ticketCost) {
    return 'NO'
  }
  for (let item of people) {
      if (item == 25) {
        clercMoney += ticketCost;
        status = true;
      } else {
        if (item <= ticketCost + clercMoney) {
          clercMoney += ticketCost;
          status = true;
        } else {
          status = false;
        }
      }
  }
  return status ? 'YES' : 'NO';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
